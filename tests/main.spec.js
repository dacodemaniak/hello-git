describe(`Main`, {
    /**
     * Check Main instanciation
     * @param {} 
    */ 
    it('Should instanciate Main', () => {
        const main = new Main()
        expect(main).toBeInstanceOf(Main)
    })

    it(`Should store 'Hello Git`, () => {
        const main = new Main()
        expect(main.hello).toBe('Hello Git')
    })
})