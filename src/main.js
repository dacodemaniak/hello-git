/**
 * main.js Base class as entry point
 * @version 1.0.0 (2023-10-10)
 * @author Jean-Luc Aubert
 */
export class Main {
    hello = ''
    constructor() {
        this.hello = 'Hello Git'
    }
}

// Self callable function to run the Main class
let app
(function () {
    app = new Main()
})()
